<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExcercisesUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<200; $i++):
            app('db')->insert(
                'INSERT INTO `excercises_users` (`user_id`, `excercise_id`, `domain_id`, `score`, `created_at`) VALUES (?, ?, ?, ?, ?)',
                [
                    mt_rand(1, 3),
                    $i,
                    mt_rand(1, 4),
                    mt_rand(1, 10000),
                    \Carbon\Carbon::now()->modify('-' . mt_rand(0, 1000) . ' hours')
                ]);
        endfor;
    }
}
