## Installation ##

cp .env.example .env

docker-compose up -d

docker-compose exec app composer install

docker-compose exec app php artisan migrate --seed


## Usage ##

Go to

[http://localhost:8080/profile/history/1](http://localhost:8080/profile/history/1)

[http://localhost:8080/profile/history/2](http://localhost:8080/profile/history/2)

[http://localhost:8080/profile/history/3](http://localhost:8080/profile/history/3)

[http://localhost:8080/profile/history/4](http://localhost:8080/profile/history/4)
