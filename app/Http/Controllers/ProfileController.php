<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function history($domain_id, Request $request)
    {
        $user_id = $request->user()->id;
        $history = app('db')->select(
'SELECT `score`, `created_at` as "date"
FROM `excercises_users`
WHERE `user_id`=?
  AND `domain_id`=?
ORDER BY `created_at` DESC
LIMIT 12', [$user_id, $domain_id]);
        return response()->json(['history' => $history]);
    }
}
